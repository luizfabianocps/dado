package br.com.itau;

public class Imprimir {

    public static void imprimir(Resultado resultado){

        for (int numero:resultado.getNumeros() ) {
            System.out.print(numero);
            System.out.print(", ");
        }

        System.out.println(resultado.soma());
    }

    public static void imprimir(Resultado[] resultados){
        for (Resultado results : resultados){
            imprimir(results);
        }

    }

}
