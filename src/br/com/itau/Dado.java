package br.com.itau;

import java.util.Random;

public class Dado {

    private int lados;
    private Random sorteio = new Random();


    public int getLados() {
        return lados;
    }

    public void setLados(int lados) {
        this.lados = lados;
    }

    public Random getSorteio() {

        return sorteio;
    }

    public void setSorteio(Random sorteio) {
        this.sorteio = sorteio;
    }

    public Dado(int lados){
        this.lados = lados;

    }

    //Regra de negócio
    public int sorteio() {
        return sorteio.nextInt(lados) + 1;
    }


}
