package br.com.itau;

public class RealizaSorteio {

    public static Resultado sorteadorDeGrupo(Dado dado, int quantidadeNumerosSortear){
        int [] numeros = new int[quantidadeNumerosSortear];

        for (int i = 0; i < quantidadeNumerosSortear; i++) {
            numeros[i] = dado.sorteio();
        }

        return new Resultado(numeros);
    }

    public static Resultado[] sorteadorDeGrupos(Dado dado, int quantidadeNumeroSortear, int quantidadeDeGrupos){
        Resultado[] resuls = new Resultado[quantidadeDeGrupos];

        for (int i = 0; i<quantidadeDeGrupos; i++){
            resuls[i] = sorteadorDeGrupo(dado, quantidadeNumeroSortear);
        }

        return resuls;

    }


}
