package br.com.itau;

public class Resultado {

    private int[] numeros;

    public Resultado(int[] numeros) {
        this.numeros = numeros;
    }

    public int[] getNumeros() {

        return numeros;
    }

    public void setNumeros(int[] numeros) {
        this.numeros = numeros;
    }

    public int soma(){
        int contador =0;

        for (int somar:numeros ){
            contador = contador + somar;
        }

        return contador;
    }
}
